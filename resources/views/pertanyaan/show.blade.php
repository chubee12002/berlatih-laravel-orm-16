@extends('layout.master')
@section('titleheader')
Welcome Home
@endsection
@section('content')
<div class="mt-3 ml-3">
    <h2>Tampilkan Pertanyaan {{$pertanyaan->id}}</h2>
    <h4>{{$pertanyaan->judul}}</h4>
    <p>{{$pertanyaan->isi}}</p>
</div>
@endsection
